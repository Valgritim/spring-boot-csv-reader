package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Personne;
import com.example.demo.services.PersonneService;


@RestController
public class PersonneRestController {
	
	@Autowired
	private PersonneService personneService;

    @RequestMapping("/personnes")
    public List<Personne> getPersonnes() {
    	
    	List<Personne> personneList = personneService.findAll();
        
        return personneList;
    }

}
