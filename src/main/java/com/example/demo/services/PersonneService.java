package com.example.demo.services;


import java.util.ArrayList;
import java.util.List;

import com.example.demo.model.Personne;

public interface PersonneService {
	
	List<Personne> findAll();

}
